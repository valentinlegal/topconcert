# TopConcert

Un site web permettant la réservation (fictive) de place pour des concerts de musique. Il s'agit de mon deuxième site web développé à l'occasion d'un projet universitaire (2018).

![TopConcert cover](./img/cover.png)

## À propos du projet

- **Nature :** projet universitaire
- **Date :** octobre 2018
- **Description :** il s'agit d'un site web permettant la réservation (fictive) de place pour des concerts de musique. Les administrateurs du site ont accès à un tableau bord. Ces derniers peuvent ajouter, modifier et supprimer des données sur le site (concert, groupe, genre musical, etc.)

## Technologies utilisées

- PHP
- SQL
- HTML
- CSS
- JS

## En savoir plus

Pour en savoir plus, un PDF nommé `compte_rendu.pdf` et un backlog nommé `user_story.pdf` sont disponibles dans ce répertoire.

## Information supplémentaire

Pour accéder au tableau de bord des administrateurs :
- **Email :** admin@topconcert.fr
- **Mot de passe :** admin