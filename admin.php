<?php 

session_start();

//Vérifie l'utilisateur est bien connecté en tant qu'admin
if (!isset($_SESSION['typeUtilisateur']) && $_SESSION['typeUtilisateur'] != "admin") {
	header("Location:index.php");
}

//Connexion à la base de données
include 'connexion.php';
$connexion = connexionBd();

//Ajout du fichier fonctions.php
include 'fonctions.php';

//Sélectionner tous les concerts de la BD
$sql0 = "SELECT * FROM groupes ORDER BY nom ASC";
$info0 = $connexion->query($sql0);
$resultat0 = $info0->fetchALL(PDO::FETCH_OBJ);

//Sélectionner tous les concerts de la BD
$sql1 = "
SELECT C.idConcert as idConcert, G.nom as nom, C.lieu as lieu, C.ville as ville, C.date as date, C.heure as heure, C.prix as prix FROM concerts C JOIN groupes G ON G.idGroupe = C.idGroupe ORDER BY C.idConcert DESC";
$info1 = $connexion->query($sql1);
$resultat1 = $info1->fetchALL(PDO::FETCH_OBJ);

//Sélectionner tous les groupes de la BD
$sql4 = "SELECT nom, idGroupe FROM groupes ORDER BY nom ASC";
$info4 = $connexion->query($sql4);
$resultat4 = $info4->fetchALL(PDO::FETCH_OBJ);


//***** Ajout d'un concert *****//

$error_validation = $error_groupe = $error_lieu = $error_ville = $error_date = $error_heure = $error_prix = "";
$groupe = $lieu = $ville = $date = $heure = $prix = "";

if(isset($_POST['send-concert'])){

	//Groupe
	if (empty($_POST['groupe']))
		$error_groupe = "Le groupe doit être renseigné.";
	else
		$groupe = test_input($_POST["groupe"]);
    //Lieu
	if (empty($_POST['lieu']))
		$error_lieu = "Le lieu doit être renseigné.";
	else
		$lieu = ucwords(test_input($_POST["lieu"]));
	//Ville
	if (empty($_POST['ville']))
		$error_ville = "La ville doit être renseignée.";
	else
		$ville = ucwords(test_input($_POST["ville"]));
	//Date
	if (empty($_POST['date']))
		$error_date = "La date doit être renseignée.";
	elseif ($_POST['date'] < date('Y-m-d')) {
		$error_date = "La date doit être <b>égale ou supérieure</b> à celle d'<b>aujourd'hui</b>.";
	}
	else
		$date = test_input($_POST["date"]);
	//Heure
	if (empty($_POST['heure']))
		$error_heure = "L'heure doit être renseignée.";
	else
		$heure = test_input($_POST["heure"]);
	//Prix
	if (empty($_POST['prix']))
		$error_prix = "Le prix doit être renseigné.";
	elseif (!preg_match("/^[0-9]*[.]{1}[0-9]*$/", $_POST['prix']) && !preg_match("/^[0-9]*$/", $_POST['prix']))
		$error_prix = "Le prix doit contenir uniquement des <b>chiffres</b> et <b>un seul point</b>.";
	else
		$prix = test_input($_POST["prix"]);

	if(!empty($_POST['groupe']) && !empty($_POST['lieu']) && !empty($_POST['ville']) && !empty($_POST['date']) && !empty($_POST['date']) && !empty($_POST['heure']) && !empty($_POST['prix'])){

        //Vérifie que le concert n'existe pas déjà
		$sql2 = "SELECT * FROM concerts WHERE idGroupe = $groupe AND lieu = '$lieu' AND ville = '$ville' AND date = '$date' AND heure = '$heure'";
		$info2 = $connexion->query($sql2);
		$resultat2 = $info2->fetchALL(PDO::FETCH_OBJ);

		if (empty($resultat2)) {
			$sql3 = "INSERT INTO concerts VALUES (NULL, $groupe, '$lieu', '$ville', '$date', '$heure', $prix)";
			$count=$connexion->exec($sql3);
			if ($count != 0) {
				$groupe = $lieu = $ville = $date = $heure = $prix = "";
				echo "<script type=\"text/javascript\">";
				echo "alert(\"Le concert a bien été enregistré !\");";
				echo "document.location.href=\"admin.php\";";
				echo "</script>";
			}
			else {
				$error_validation = "Échec. Veuillez réessayer.";
			}
		}
		else {
			$error_validation = "Ce concert existe déjà. Vérifiez les données saisies.";
		}
	}

}

//***** Ajout d'un groupe *****//

$error_nom = $error_genre = $error_bio = $error_avatar = $error_cover = "";
$nom = $genre = $bio = $avatar = $cover = "";

if(isset($_POST['send-groupe'])){

	//Nom
	if (empty($_POST['nom']))
		$error_nom = "Le nom doit être renseigné.";
	else
		$nom = ucwords(test_input($_POST["nom"]));
	//Genre
	if (empty($_POST['genre']))
		$error_genre = "Le genre doit être renseigné.";
	else
		$genre = ucwords(test_input($_POST["genre"]));
	//Bio
	if (empty($_POST['bio']))
		$error_bio = "La description doit être renseignée.";
	else
		$bio = test_input($_POST["bio"]);

	//Avatar
	if (empty($_FILES['avatar']["tmp_name"])) {
		$error_avatar = "La photo de profil doit être renseignée.";
		$_FILES['avatar']["tmp_name"] = "";
	}
	elseif (getimagesize($_FILES["avatar"]["tmp_name"])['mime'] != "image/jpeg") {
		$error_avatar = "L'extension de l'image doit être en <b>.jpeg</b>, <b>.JPEG</b>, <b>.jpg</b> ou <b>.JPG</b>.";
		$_FILES['avatar']["tmp_name"] = "";
	}
	else
		$avatar = $_FILES["avatar"]["tmp_name"];

	//Cover
	if (empty($_FILES['cover']["tmp_name"])) {
		$error_cover = "La photo de couverture doit être renseignée.";
		$_FILES['cover']["tmp_name"] = "";
	}
	elseif (getimagesize($_FILES["cover"]["tmp_name"])['mime'] != "image/jpeg") {
		$error_cover = "L'extension de l'image doit être en <b>.jpeg</b>, <b>.JPEG</b>, <b>.jpg</b> ou <b>.JPG</b>.";
		$_FILES['cover']["tmp_name"] = "";
	}
	else
		$cover = $_FILES["cover"]["tmp_name"];


	if(!empty($_POST['nom']) && !empty($_POST['genre']) && !empty($_POST['bio']) && !empty($_FILES['avatar']["tmp_name"]) && !empty($_FILES['cover']["tmp_name"])) {

        //Vérifie que le groupe n'existe pas déjà
		$sql2 = "SELECT * FROM groupes WHERE nom = '$nom'";
		$info2 = $connexion->query($sql2);
		$resultat2 = $info2->fetchALL(PDO::FETCH_OBJ);
        
        $id_utilisateur = $_SESSION['id'];
        
        //$sql = "SELECT M.nom as nom_utilisateur, C.nom as nom_concert FROM pannier P JOIN membres M ON P.id_utilisateur = M.id_utilisateur JOIN concert C ON P.id_evenement = C.idConcert WHERE //P.id_utilisateur = $id_utilisateur";
		//$sql = $connexion->query($sql);
		//$sql = $sql->fetchALL(PDO::FETCH_OBJ);
        
        

		if (empty($resultat2)) {

			//Copie des images sur le serveur
			$nomGroupe = strtolower(preg_replace('/ /', '-', $nom));
			$nomAvatar = $nomGroupe."-avatar.jpg";
			move_uploaded_file($_FILES["avatar"]["tmp_name"], "img/groupes/".$nomAvatar);
			$nomCover = $nomGroupe."-cover.jpg";
			move_uploaded_file($_FILES["cover"]["tmp_name"], "img/groupes/".$nomCover);

			$sql3 = "INSERT INTO groupes VALUES (NULL, '$nom', '$genre', '$bio', '$nomAvatar', '$nomCover')";
			$count=$connexion->exec($sql3);
			if ($count != 0) {
				$nom = $genre = $bio = $avatar = $cover = "";
				echo "<script type=\"text/javascript\">";
				echo "alert(\"Le groupe a bien été enregistré !\");";
				echo "document.location.href=\"admin.php\";";
				echo "</script>";
			}
			else {
				$error_validation = "Échec. Veuillez réessayer.";
			}
		}
		else {
			$error_validation = "Ce groupe existe déjà. Vérifiez les données saisies.";
		}
	}

}

//***** Modification d'un concert *****//

if(!empty($_GET['idConcert'])) {

	$idConcert = $_GET['idConcert'];

	$modif1 = "SELECT G.nom as nom, G.idGroupe as idGroupe, C.lieu as lieu, C.ville as ville, C.date as date, C.heure as heure, C.prix as prix FROM concerts C JOIN groupes G ON G.idGroupe = C.idGroupe WHERE idConcert = $idConcert";
	$modif1 = $connexion->query($modif1);
	$modif1 = $modif1->fetch(PDO::FETCH_OBJ);

	$groupe = $modif1->nom;
	$idGroupe = $modif1->idGroupe;
    $lieu = $modif1->lieu;
	$ville = $modif1->ville;
	$date = $modif1->date;
	$heure = $modif1->heure;
	$prix = $modif1->prix;

}

if(isset($_POST['send-modif-concert'])) {
	$idConcert = $_POST["concert"];

	//Groupe
	if (empty($_POST['groupe'])) {
		$error_groupe = "Le groupe doit être renseigné.";
	}
	else {
		$idGroupe = test_input($_POST["groupe"]);
		$groupe = "SELECT nom FROM groupes WHERE idGroupe = $idGroupe";
		$groupe = $connexion->query($groupe);
		$groupe = $groupe->fetch(PDO::FETCH_OBJ);
		$groupe = $groupe->nom;
	}
    //Lieu
	if (empty($_POST['lieu']))
		$error_lieu = "Le lieu doit être renseigné.";
	else
		$lieu = ucwords(test_input($_POST["lieu"]));
	//Ville
	if (empty($_POST['ville']))
		$error_ville = "La ville doit être renseignée.";
	else
		$ville = ucwords(test_input($_POST["ville"]));
	//Date
	if (empty($_POST['date']))
		$error_date = "La date doit être renseignée.";
	elseif ($_POST['date'] < date('Y-m-d')) {
		$error_date = "La date doit être <b>égale ou supérieure</b> à celle d'<b>aujourd'hui</b>.";
	}
	else
		$date = test_input($_POST["date"]);
	//Heure
	if (empty($_POST['heure']))
		$error_heure = "L'heure doit être renseignée.";
	else
		$heure = test_input($_POST["heure"]);
	//Prix
	if (empty($_POST['prix']))
		$error_prix = "Le prix doit être renseigné.";
	elseif (!preg_match("/^[0-9]*[.]{1}[0-9]*$/", $_POST['prix']) && !preg_match("/^[0-9]*$/", $_POST['prix']))
		$error_prix = "Le prix doit contenir uniquement des <b>chiffres</b> et <b>un seul point</b>.";
	else
		$prix = test_input($_POST["prix"]);

	if(!empty($_POST['groupe']) && !empty($_POST['lieu']) && !empty($_POST['ville']) && !empty($_POST['date']) && !empty($_POST['heure']) && !empty($_POST['prix'])){

        //Vérifie que le concert n'existe pas déjà
		$sql2 = "SELECT * FROM concerts WHERE idGroupe = $idGroupe AND ville = '$lieu' AND ville = '$ville' AND date = '$date' AND heure = '$heure' AND prix = '$prix'";
		$info2 = $connexion->query($sql2);
		$resultat2 = $info2->fetchALL(PDO::FETCH_OBJ);

		if (empty($resultat2)) {
			$sql3 = "UPDATE concerts SET idGroupe=$idGroupe, lieu='$lieu', ville='$ville', date='$date', heure='$heure', prix='$prix' WHERE idConcert = $idConcert";
			$count=$connexion->exec($sql3);
			if ($count != 0) {
				$idConcert = $idGroupe = $groupe = $lieu = $ville = $date = $heure = $prix = "";
				echo "<script type=\"text/javascript\">";
				echo "alert(\"Le concert a bien été modifié !\");";
				echo "document.location.href=\"admin.php\";";
				echo "</script>";
			}
			else {
				$error_validation = "Échec. Veuillez réessayer.";
			}
		}
		else {
			$error_validation = "Ce concert existe déjà. Vérifiez les données saisies.";
		}
	}

}

//***** Modification d'un groupe *****//

if(!empty($_GET['idGroupe'])) {

	$idGroupe = $_GET['idGroupe'];

	$modif2 = "SELECT * FROM groupes WHERE idGroupe = $idGroupe";
	$modif2 = $connexion->query($modif2);
	$modif2 = $modif2->fetch(PDO::FETCH_OBJ);

	$nom = $modif2->nom;
	$genre = $modif2->genre;
	$bio = $modif2->bio;
	$avatar = $modif2->imgAvatar;
	$cover = $modif2->imgCover;

	$error_avatar = "Laisser vide si vous ne souhaitez pas changer l'image.";
	$error_cover = "Laisser vide si vous ne souhaitez pas changer l'image.";

}

if(isset($_POST['send-modif-groupe'])) {
	$idGroupe = $_POST["groupe"];

	//Nom
	if (empty($_POST['nom']))
		$error_nom = "Le nom doit être renseigné.";
	else
		$nom = ucwords(test_input($_POST["nom"]));
	//Genre
	if (empty($_POST['genre']))
		$error_genre = "Le genre doit être renseigné.";
	else
		$genre = ucwords(test_input($_POST["genre"]));
	//Bio
	if (empty($_POST['bio']))
		$error_bio = "La description doit être renseignée.";
	else
		$bio = test_input($_POST["bio"]);

	//Avatar
	if (empty($_FILES['avatar']["tmp_name"])) {
		$_FILES['avatar']["tmp_name"] = "";
		$avatar = test_input($_POST['avatar2']);
		$error_avatar = "Laisser vide si vous ne souhaitez pas changer l'image.";
	}
	elseif (getimagesize($_FILES["avatar"]["tmp_name"])['mime'] != "image/jpeg") {
		$error_avatar = "L'extension de l'image doit être en <b>.jpeg</b>, <b>.JPEG</b>, <b>.jpg</b> ou <b>.JPG</b>.";
		$_FILES['avatar']["tmp_name"] = "";
		$avatar = test_input($_POST['avatar2']);
	}
	else
		$avatar = $_FILES["avatar"]["tmp_name"];

	//Cover
	if (empty($_FILES['cover']["tmp_name"])) {
		$_FILES['cover']["tmp_name"] = "";
		$cover = test_input($_POST['cover2']);
		$error_cover = "Laisser vide si vous ne souhaitez pas changer l'image.";
	}
	elseif (getimagesize($_FILES["cover"]["tmp_name"])['mime'] != "image/jpeg") {
		$error_cover = "L'extension de l'image doit être en <b>.jpeg</b>, <b>.JPEG</b>, <b>.jpg</b> ou <b>.JPG</b>.";
		$_FILES['cover']["tmp_name"] = "";
		$cover = test_input($_POST['cover2']);
	}
	else
		$cover = $_FILES["cover"]["tmp_name"];

	if(!empty($_POST['nom']) && !empty($_POST['genre']) && !empty($_POST['bio'])) {

        //Vérifie que le groupe n'existe pas déjà
		$sql2 = "SELECT * FROM groupes WHERE nom = '$nom' AND genre = '$genre' AND bio='$bio' AND imgAvatar = 'avatar' AND imgCover = '$cover'";
		$info2 = $connexion->query($sql2);
		$resultat2 = $info2->fetchALL(PDO::FETCH_OBJ);

		if (empty($resultat2)) {

			//Copie des images sur le serveur
			$nomGroupe = strtolower(preg_replace('/ /', '-', $nom));

			if (!empty($_FILES['avatar']["tmp_name"])) {
				$nomAvatar = $nomGroupe."-avatar.jpg";
				move_uploaded_file($_FILES["avatar"]["tmp_name"], "img/groupes/".$nomAvatar);
			}
			else {
				$nomAvatar = $avatar;
			}

			if (!empty($_FILES['cover']["tmp_name"])) {
				$nomCover = $nomGroupe."-cover.jpg";
				move_uploaded_file($_FILES["cover"]["tmp_name"], "img/groupes/".$nomCover);
			}
			else {
				$nomCover = $cover;
			}

			//Modifie les valeurs
			$sql3 = "UPDATE groupes SET idGroupe=$idGroupe, nom='$nom', genre='$genre', bio='$bio', imgAvatar='$nomAvatar', imgCover='$nomCover' WHERE idGroupe = $idGroupe";	
			$count=$connexion->exec($sql3);
			if (!empty($_FILES['avatar']["tmp_name"]) || !empty($_FILES['cover']["tmp_name"])) {
				$count = 1;
			}
			if ($count != 0) {
				$idGroupe = $nomAvatar = $nomCover = $nom = $genre = $bio = $avatar = $cover = "";
				echo "<script type=\"text/javascript\">";
				echo "alert(\"Le groupe a bien été modifié !\");";
				echo "document.location.href=\"admin.php\";";
				echo "</script>";
			}
			else {
				$error_validation = "Échec. Veuillez réessayer.";
			}
		}
		else {
			$error_validation = "Ce groupe existe déjà. Vérifiez les données saisies.";
		}
	}

}

//***** Supprimer concert *****//

if(!empty($_GET['idConcert_suppr'])) {
	$idConcert = $_GET['idConcert_suppr'];
	$delete = "DELETE FROM concerts WHERE idConcert = $idConcert";
	$count=$connexion->exec($delete);
	if ($count != 0) {
		$idConcert = "";
		echo "<script type=\"text/javascript\">";
		echo "alert(\"Le concert a bien été supprimé !\");";
		echo "document.location.href=\"admin.php\";";
		echo "</script>";
	}
	else {
		echo $count;
	}
}

//***** Supprimer groupe *****//

if(!empty($_GET['idGroupe_suppr'])) {
	$idGroupe = $_GET['idGroupe_suppr'];

	//Supprime les concerts associés aux groupes
	$deleteConcerts = "DELETE FROM concerts WHERE idGroupe = $idGroupe";
	$deleteConcerts=$connexion->exec($deleteConcerts);

	//Supprime du serveur les images associés aux groupes
	$selectImg = "SELECT imgAvatar, imgCover FROM groupes WHERE idGroupe = $idGroupe";
	$selectImg = $connexion->query($selectImg);
	$selectImg = $selectImg->fetch(PDO::FETCH_OBJ);

	$deleteAvatar = 'img/groupes/'.$selectImg->imgAvatar;
	if(file_exists($deleteAvatar))
		unlink($deleteAvatar);

	$deleteCover = 'img/groupes/'.$selectImg->imgCover;
	if(file_exists($deleteCover))
		unlink($deleteCover);

	//Supprime le groupe
	$deleteGroupe = "DELETE FROM groupes WHERE idGroupe = $idGroupe";
	$deleteGroupe=$connexion->exec($deleteGroupe);

	if (($deleteConcerts != 0) && ($deleteGroupe != 0)) {
		$idConcert = "";
		echo "<script type=\"text/javascript\">";
		echo "alert(\"Le groupe et ses concerts associés ont bien été supprimé !\");";
		echo "document.location.href=\"admin.php\";";
		echo "</script>";
	}
	else {
		echo $count;
	}
}

?>



<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Espace admin | Top Concert</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link rel="shortcut icon" href="img/favicon.png">
	<link rel="stylesheet" href="css/style-admin.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
</head>
<body>

	<header>
		<?php require('header.php') ?>
	</header>

	<div class="container">

		<!-- TABLEAU CONCERTS -->
		<section>
			<h2>Concerts :</h2>
			<form action="admin.php" method="post">
				<p onclick="javascript:on('ajouter-concert')" title="Ajouter un concert à la base de données">AJOUTER UN CONCERT</p>
				<table>
					<tr>
						<th>ID Concert</th>
						<th>Groupe</th>
						<th style="max-width:100px;">Lieu</th>
						<th>Ville</th>
						<th>Date</th>
						<th>Heure</th>
						<th>Prix</th>
						<th colspan="2">Action</th>
					</tr>
					<?php foreach ($resultat1 as $key => $value): ?>
						<tr>
							<td><?=$value->idConcert?></td>
							<td><?=$value->nom?></td>
							<td style="max-width:100px;"><?=$value->lieu?></td>
							<td><?=$value->ville?></td>
							<td><?=formater_date($value->date)?></td>
							<td><?=formater_heure($value->heure)?></td>
							<td><?=$value->prix?>€</td>
							<td><a class="action" href="admin.php?idConcert=<?=$value->idConcert?>" title="Modifier le concert">Modifier</a></td>
							<td><p class="action" title="Supprimer le concert" onclick="javascript:confirmSupprConcert(<?=$value->idConcert?>)">Supprimer</p></td>
						</tr>
					<?php endforeach ?>
				</table>
			</form>
		</section>

		<!-- TABLEAU GROUPES -->
		<section>
			<h2>Groupes :</h2>
			<form action="admin.php" method="post">
				<p onclick="javascript:on('ajouter-groupe')" title="Ajouter un groupe à la base de données">AJOUTER UN GROUPE</p>	
				<table>
					<tr>
						<th>ID Groupe</th>
						<th>Nom</th>
						<th>Genre</th>
						<th colspan="2">Action</th>
					</tr>
					<?php foreach ($resultat0 as $key => $value): ?>
						<tr>
							<td><?=$value->idGroupe?></td>
							<td><?=$value->nom?></td>
							<td><?=$value->genre?></td>
							<td><a class="action" href="admin.php?idGroupe=<?=$value->idGroupe?>" title="Modifier le groupe">Modifier</a></td>
							<td><p class="action" title="Supprimer le concert" onclick="javascript:confirmSupprGroupe(<?=$value->idGroupe?>)">Supprimer</p></td>
						</tr>
					<?php endforeach ?>
				</table>
			</form>
		</section>

	</div>

	<!-- OVERLAY AJOUT CONCERT -->
	<form id="ajouter-concert" class="ajouter" action="admin.php" method="post">
		<h3>Ajouter un concert :</h3>
		<table>
			<tr>
				<td><label>Groupe :</label></td>
				<td>
					<select name="groupe">
						<option value="">---</option>
						<?php foreach ($resultat4 as $key => $value): ?>
							<option value="<?=$value->idGroupe?>"><?=$value->nom?></option>
						<?php endforeach ?>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_groupe?></td>
			</tr>
			<tr>
				<td><label>Lieu :</label></td>
				<td><input type="text" name="lieu" maxlength="64" value="<?=$lieu?>" placeholder="Ex : La Sirène"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_lieu?></td>
			</tr>
			<tr>
				<td><label>Ville :</label></td>
				<td><input type="text" name="ville" maxlength="64" value="<?=$ville?>" placeholder="Ex : La Rochelle"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_ville?></td>
			</tr>
			<tr>
				<td><label>Date :</label></td>
				<td><input type="date" value="<?=$date?>" name="date"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_date?></td>
			</tr>
			<tr>
				<td><label>Heure :</label></td>
				<td><input type="time" value="<?=$heure?>" name="heure"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_heure?></td>
			</tr>
			<tr>
				<td><label>Prix :</label></td>
				<td><input type="text" name="prix" value="<?=$prix?>" placeholder="Ex : 5.5"><label style="color: black;">€</label></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_prix?></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="send-concert" value="VALIDER"><input type="reset" value="ANNULER" onclick="javascript:off('ajouter-concert')"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_validation?></td>
			</tr>
		</table>
	</form>

	<!-- OVERLAY MODIFICATION CONCERT -->
	<form id="modifier-concert" class="ajouter" action="admin.php" method="post">
		<h3>Modifier un concert :</h3>
		<table>
			<tr>
				<td><label>Groupe :</label></td>
				<td>
					<select name="groupe">
						<option value="<?=$idGroupe?>"><?=$groupe?></option>
						<?php foreach ($resultat4 as $key => $value): ?>
							<option value="<?=$value->idGroupe?>"><?=$value->nom?></option>
						<?php endforeach ?>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_groupe?></td>
			</tr>
			<tr>
				<td><label>Lieu :</label></td>
				<td><input type="text" name="lieu" maxlength="64" value="<?=$lieu?>" placeholder="Ex : La Sirène"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_lieu?></td>
			</tr>
			<tr>
				<td><label>Ville :</label></td>
				<td><input type="text" name="ville" maxlength="64" value="<?=$ville?>" placeholder="Ex : La Rochelle"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_ville?></td>
			</tr>
			<tr>
				<td><label>Date :</label></td>
				<td><input type="date" value="<?=$date?>" name="date"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_date?></td>
			</tr>
			<tr>
				<td><label>Heure :</label></td>
				<td><input type="time" value="<?=formater_heure($heure)?>" name="heure"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_heure?></td>
			</tr>
			<tr>
				<td><label>Prix :</label></td>
				<td><input type="text" name="prix" value="<?=$prix?>" placeholder="Ex : 5.5"><label style="color: black;">€</label></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_prix?></td>
			</tr>
			<tr>
				<td><input type="hidden" value="<?=$idConcert?>" name="concert"></td>
				<td><input type="submit" name="send-modif-concert" value="VALIDER"><input type="reset" value="ANNULER" onclick="javascript:off('modifier-concert')"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_validation?></td>
			</tr>
		</table>
	</form>

	<!-- OVERLAY AJOUT GROUPE -->
	<form id="ajouter-groupe" class="ajouter" action="admin.php" method="post" enctype="multipart/form-data">
		<h3>Ajouter un groupe :</h3>
		<table>
			<tr>
				<td><label>Nom du groupe :</label></td>
				<td><input type="text" name="nom" value="<?=$nom?>" maxlength="64" placeholder="Ex : Lysistrata"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_nom?></td>
			</tr>

			<tr>
				<td><label>Genre :</label></td>
				<td><input type="text" name="genre" value="<?=$genre?>" maxlength="64" placeholder="Ex : Rock"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_genre?></td>
			</tr>

			<tr>
				<td><label>Description :</label></td>
				<td><textarea name="bio" cols="30" rows="5" maxlength="1000" placeholder="Décrivez le groupe en 1000 caractères maximum..."><?=$bio?></textarea></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_bio?></td>
			</tr>

			<tr>
				<td><label>Photo de profil / logo du groupe :</label></td>
				<td><input type="file" name="avatar" accept="image/jpeg"/></td>
			</tr>
			<tr>
				<td class="info">Extension : <b>.jpeg</b>, <b>.JPEG</b>, <b>.jpg</b> ou <b>.JPG</b>
				<td class="error"><?=$error_avatar?></td>
			</tr>

			<tr>
				<td><label>Photo de couverture du groupe :</label></td>
				<td><input type="file" name="cover" accept="image/jpeg" /></td>
			</tr>
			<tr>
				<td class="info">Extension : <b>.jpeg</b>, <b>.JPEG</b>, <b>.jpg</b> ou <b>.JPG</b>
				<td class="error"><?=$error_cover?></td>
			</tr>


			<tr>
				<td></td>
				<td><input type="submit" name="send-groupe" value="VALIDER"><input type="reset" value="ANNULER" onclick="javascript:off('ajouter-groupe')"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_validation?></td>
			</tr>
			<tr>
				<td colspan="2">
					<a href="https://www.iloveimg.com/fr/convertir-en-jpg" target="_blank">Convertir une image en <b>.jpg</b> <i class="fas fa-external-link-alt"></i></a>
				</td>
			</tr>
		</table>
	</form>

	<!-- OVERLAY MODIFICATION GROUPE -->
	<form id="modifier-groupe" class="ajouter" action="admin.php" method="post" enctype="multipart/form-data">
		<h3>Modifier un groupe :</h3>
		<table>
			<tr>
				<td><label>Nom du groupe :</label></td>
				<td><input type="text" name="nom" value="<?=$nom?>" maxlength="64" placeholder="Ex : Lysistrata"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_nom?></td>
			</tr>

			<tr>
				<td><label>Genre :</label></td>
				<td><input type="text" name="genre" value="<?=$genre?>" maxlength="64" placeholder="Ex : Rock"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_genre?></td>
			</tr>

			<tr>
				<td><label>Description :</label></td>
				<td><textarea name="bio" cols="30" rows="5" maxlength="1000" placeholder="Décrivez le groupe en 1000 caractères maximum..."><?=$bio?></textarea></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_bio?></td>
			</tr>

			<tr>
				<td><label>Photo de profil / logo du groupe :</label></td>
				<td><input type="file" name="avatar" accept="image/jpeg"/></td>
				<td><input type="hidden" name="avatar2" value="<?=$avatar?>"></td>
			</tr>
			<tr>
				<td class="info">Extension : <b>.jpeg</b>, <b>.JPEG</b>, <b>.jpg</b> ou <b>.JPG</b>
				<td class="error"><?=$error_avatar?></td>
			</tr>

			<tr>
				<td><label>Photo de couverture du groupe :</label></td>
				<td><input type="file" name="cover" accept="image/jpeg" /></td>
				<td><input type="hidden" name="cover2" value="<?=$cover?>"></td>
			</tr>
			<tr>
				<td class="info">Extension : <b>.jpeg</b>, <b>.JPEG</b>, <b>.jpg</b> ou <b>.JPG</b>
				<td class="error"><?=$error_cover?></td>
			</tr>


			<tr>
				<td><input type="hidden" value="<?=$idGroupe?>" name="groupe"></td>
				<td><input type="submit" name="send-modif-groupe" value="VALIDER"><input type="reset" value="ANNULER" onclick="javascript:off('modifier-groupe')"></td>
			</tr>
			<tr>
				<td></td>
				<td class="error"><?=$error_validation?></td>
			</tr>
			<tr>
				<td colspan="2">
					<a href="https://www.iloveimg.com/fr/convertir-en-jpg" target="_blank">Convertir une image en <b>.jpg</b> <i class="fas fa-external-link-alt"></i></a>
				</td>
			</tr>
		</table>
	</form>

	<div id="filtre-noir" onclick="javascript:offAll()"></div>



	<footer>
		<?php require('footer.php') ?>
	</footer>

	<script type="text/javascript" src="js/comp_admin.js"></script>

</body>
</html>




<?php 

if(isset($_POST['send-concert'])){
	echo "<script type=\"text/javascript\">";
	echo "document.getElementById(\"ajouter-concert\").style.display = \"inline-block\"; document.getElementById(\"filtre-noir\").style.display = \"block\";";
	echo "</script>";
}

if(isset($_POST['send-groupe'])){
	echo "<script type=\"text/javascript\">";
	echo "document.getElementById(\"ajouter-groupe\").style.display = \"inline-block\"; document.getElementById(\"filtre-noir\").style.display = \"block\";";
	echo "</script>";
}

if(!empty($_GET['idConcert']) || isset($_POST['send-modif-concert'])) {
	echo "<script type=\"text/javascript\">";
	echo "document.getElementById(\"modifier-concert\").style.display = \"inline-block\"; document.getElementById(\"filtre-noir\").style.display = \"block\";";
	echo "</script>";
}

if(!empty($_GET['idGroupe']) || isset($_POST['send-modif-groupe'])) {
	echo "<script type=\"text/javascript\">";
	echo "document.getElementById(\"modifier-groupe\").style.display = \"inline-block\"; document.getElementById(\"filtre-noir\").style.display = \"block\";";
	echo "</script>";
}

?>