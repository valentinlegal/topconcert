<?php 

//Vérifie les champs input
function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = addslashes($data);
	$data = strip_tags($data);
	$data = htmlspecialchars($data);
	return $data;
}

//Formater la date
function formater_date($date) {
	$date = explode("-", $date);
	$date = $date[2]."/".$date[1]."/".$date[0];
	return $date;
}

//Formater la date
function formater_heure($heure) {
	$heure = explode(":", $heure);
	$heure = $heure[0].":".$heure[1];
	return $heure;
}

//Tronquer texte
function tronquer_texte($texte, $longueur_max = 100) {
	if(strlen($texte) > $longueur_max) {
		$texte = substr($texte, 0, $longueur_max);
		$texte .= "...";
	}
	return $texte;
}

?>