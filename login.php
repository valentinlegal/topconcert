<?php 

//Connexion à la base de données
include 'connexion.php';
$connexion = connexionBd();

//Vérifie les champs input
function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

$error_email = $error_password = $error_validation = $succes_validation = "";
$email = $password = "";

if(isset($_POST['send'])){

    //Email
	if (empty($_POST['email']))
		$error_email = "L'adresse e-mail doit être renseignée.";
	else
		$email = test_input($_POST["email"]);
    //Mot de passe
	if (empty($_POST['password']))
		$error_password = "Le mot de passe doit être renseigné.";
	else
		$password = hash('sha256', test_input($_POST["password"]));


	if(!empty($_POST['email']) && !empty($_POST['password'])) {

        //Vérifie si les identifiants sont bons
		$sql1 = "SELECT * FROM utilisateurs WHERE email = '$email' AND motDePasse = '$password'";
		$info1=$connexion->query($sql1);
		$resultat1=$info1->fetch(PDO::FETCH_OBJ);

		if (!empty($resultat1)) {
			//Vérifie si utilisateur est un admin et le redirige sur la bonne page
			if ($resultat1->typeUtilisateur == "admin") {
				session_start();
				$_SESSION['idUtilisateur'] = $resultat1->idUtilisateur;
				$_SESSION['typeUtilisateur'] = $resultat1->typeUtilisateur;
				header("Location:admin.php");
			}
		}
		else {
			$error_validation = "L'e-mail ou le mot de passe est incorrect. Veuillez réessayer.";
		}
	}
}



?>



<!--------------------  H T M L  -------------------->

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Connexion | Top Concert</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link rel="shortcut icon" href="./img/favicon.png">
	<link rel="stylesheet" href="css/style-index.css">
	<link rel="stylesheet" href="css/style-login.css">
</head>
<body>

	<header>
		<?php require('header.php') ?>
	</header>


	<section>
		<h2>Se connecter :</h2>
		<p>(Seuls les administrateurs du site peuvent se connecter)</p>

		<form method="post" action="login.php">
			<table>
				<tr>
					<td><label>E-mail :</label></td>
					<td><input type="email" name="email" placeholder="nom@exemple.com"></td>
				</tr>
				<tr>
					<td></td>
					<td class="error"><?=$error_email?></td>
				</tr>
				<tr>
					<td><label>Mot de passe :</label></td>
					<td><input type="password" name="password"></td>
				</tr>
				<tr>
					<td></td>
					<td class="error"><?=$error_password?></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" name="send" value="Connexion" title="Se connecter"></td>
				</tr>
				<tr>
					<td colspan="2" class="error"><?=$error_validation?></td>
				</tr>
				<tr>
					<td colspan="2" class="succes"><?=$succes_validation?></td>
				</tr>
			</table>
		</form>

	</section>


	<footer>
		<?php require('footer.php') ?>
	</footer>

</body>
</html>