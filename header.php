<head>
	<link rel="stylesheet" href="css/style-header.css">
</head>

<a id="button" class="api" href="indexApi.php" title="API">API</a>
<a href="index.php" title="Retourner à l'accueil"><img src="img/logo.png" alt="Logo"></a>
<?php if (isset($_SESSION['typeUtilisateur'])): ?>
	<a id="button" class="logout" href="logout.php" title="Se déconnecter">Déconnexion</a>
	<?php if ($_SESSION['typeUtilisateur'] == "admin"): ?>
		<a id="button" class="dashboard" href="admin.php" title="Tableau de bord">Tableau de bord</a>
	<?php endif ?>
	<?php else: ?>
		<a id="button" class="login" href="login.php" title="Se connecter">Connexion</a>
	<?php endif ?>
