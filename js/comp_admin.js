function on(id) {
	document.getElementById(id).style.display = "block";
	document.getElementById("filtre-noir").style.display = "block";
}

function off(id) {
	document.getElementById(id).style.display = "none";
	document.getElementById("filtre-noir").style.display = "none";
}

function offAll() {
	document.getElementById("filtre-noir").style.display = "none";
	document.getElementById("ajouter-concert").style.display = "none";
	document.getElementById("ajouter-groupe").style.display = "none";
	document.getElementById("modifier-concert").style.display = "none";
	document.getElementById("modifier-groupe").style.display = "none";

}

function confirmSupprConcert(id) {
	if (confirm("Voulez-vous vraiment supprimer ce concert ?")) {

		document.location.href=("admin.php?idConcert_suppr="+id);
	}
}

function confirmSupprGroupe(id) {
	if (confirm("Voulez-vous vraiment supprimer ce groupe ? (Cette action supprimera aussi tous les concerts associés au groupe)")) {

		document.location.href=("admin.php?idGroupe_suppr="+id);
	}
}