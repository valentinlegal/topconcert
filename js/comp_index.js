function on(id) {
	document.getElementById(id).style.display = "block";
	document.getElementById("filtre-noir").style.display = "block";
}

function off(id) {
	document.getElementById(id).style.display = "none";
	document.getElementById("filtre-noir").style.display = "none";
}

function offAll() {
	document.getElementById("filtre-noir").style.display = "none";
	document.getElementById("vue_concert").style.display = "none";
}