<?php

namespace app\entity;

class Concert extends Groupe {
    private $_idConcert;
    private $_idGroupe;
    private $_lieu;
    private $_ville;
    private $_date;
    private $_heure;
    private $_prix;

    /**
	 * Constructruir le concert.
	 *
	 * @param array $data
	 */
    public function __construct(array $data) {
        $this->hydrate ( $data );
    }

    /**
	 * Hydrater le constructeur.
	 *
	 * @param array $donnees
	 */
    public function hydrate(array $donnees) {
        foreach ( $donnees as $key => $value ) {
            $method = 'set' . ucfirst ( $key );

            if (method_exists ( $this, $method )) {
                $this->$method ( $value );
            }
        }
    }

    /**
	 * Obtenir l'id du concert.
	 *
	 * @return int
	 */
    public function getIdConcert() {
        return $this->_idConcert;
    }

    /**
	 * Modifier l'id du concert.
	 *
	 * @param int $_id du concert
	 */
    public function setIdConcert($_idConcert) {
        $this->_idConcert = $_idConcert;
    }

    /**
	 * Obtenir l'id du groupe.
	 *
	 * @return int
	 */
    public function getIdGroupe() {
        return $this->_idGroupe;
    }

    /**
	 * Modifier l'id du groupe.
	 *
	 * @param int $_idGroupe
	 */
    public function setIdGroupe($_idGroupe) {
        $this->_idGroupe = $_idGroupe;
    }

    /**
	 * Obtenir le lieu du concert.
	 *
	 * @return String
	 */
    public function getLieu() {
        return $this->_lieu;
    }

    /**
	 * Changer ke lieu du concert.
	 *
	 * @param String $_lieu
	 *        	du concert.
	 */
    public function setLieu($_lieu) {
        $this->_lieu = $_lieu;
    }

    /**
	 * Obtenir la ville du concert.
	 *
	 * @return String
	 */
    public function getVille() {
        return $this->_ville;
    }

    /**
	 * Modifier la ville du concert.
	 *
	 * @param String $_ville
	 *        	du concert.
	 */
    public function setVille($_ville) {
        $this->_ville = $_ville;
    }

    /**
	 * Obtenir la date du concert.
	 *
	 * @return String
	 */
    public function getDate() {
        return $this->_date;
    }

    /**
	 * Modifier la date du concert.
	 *
	 * @param String $_date
	 *        	du concert.
	 */
    public function setDate($_date) {
        $this->_date = $_date;
    }

    /**
	 * Obtenir l'heure du concert.
	 *
	 * @return String
	 */
    public function getHeure() {
        return $this->_heure;
    }

    /**
	 * Modifier l'heure du concert.
	 *
	 * @param String $_heure
	 *        	du concert.
	 */
    public function setHeure($_heure) {
        $this->_heure = $_heure;
    }

    /**
	 * Obtenir le prix du concert.
	 *
	 * @return double
	 */
    public function getPrix() {
        return $this->_prix;
    }

    /**
	 * Modifier le prix du concert.
	 *
	 * @param double $_prix
	 *        	du concert.
	 */
    public function setPrix($_prix) {
        $this->_prix = $_prix;
    }

    /**
	 * Afficher le concert.
	 *
	 * @return String
	 */
    public function __toString() {
        return "Id concert : " . $this->_idConcert . " | Id groupe : " . $this->_idGroupe . " | Lieu : " . $this->_lieu . " | Ville : " . $this->_ville . " | Date : " . $this->_date . " | Heure : " . $this->_heure . " | Prix :" . $this->_prix." €";
    }
}