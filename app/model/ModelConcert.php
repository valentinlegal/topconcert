<?php

namespace app\model;

use app\entity\Concert;

// use app\model\Model;
class ModelConcert extends Model {

    /**
	 * Construit l'appel à la base de données en fonction d'une table.
	 */
    public function __construct() {
        $this->_table = "concerts";
        parent::__construct ();
    }

    /**
	 * Sélectionne tous les concerts avec les informations du groupe.
	 *
	 * @return array $newTab contenant tous les concert.
	 */
    public function findAllApi() {
        $arrayConcert = $this->find ( array (
            "join" => "groupes ON concerts.idGroupe = groupes.idGroupe",
            "order" => "concerts.idConcert"
        ) );

        return $arrayConcert;
    }

    /**
	 * Compte le nombre de concerts dans la base de données.
	 *
	 * @return int
	 */
    public function nbConcerts() {
        return count ( $this->findAllApi () );
    }
}