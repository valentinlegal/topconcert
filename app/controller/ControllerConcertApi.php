<?php
namespace app\controller;

use \app\model\ModelConcert;



class ControllerConcertApi {

    // Attributs

    private $model;


    //Constructeur

    public function __construct() {
        $this->model = new ModelConcert();
    }


    public function getAllApi() {
        $nbhits = $this->model->nbConcerts();
        $content = $this->model->findAllApi();
        include('app/view/viewApi.php');

    }

}