<?php

session_start();

//Connexion à la base de données
include 'connexion.php';
$connexion = connexionBd();

//Ajout du fichier fonctions.php
include 'fonctions.php';

//Confirmation de la réservation
if (isset($_GET['idConcert'])) {
	$idConcert = $_GET['idConcert'];

	if (isset($_POST['send'])) {
		$email = $_POST['email'];

		$sql1 = "SELECT * FROM concerts C JOIN groupes G ON C.idGroupe = G.idGroupe WHERE idConcert = $idConcert";
		$sql1 = $connexion->query($sql1);
		$sql1 = $sql1->fetch(PDO::FETCH_OBJ);

		$nom = $sql1->nom;
		$lieu = $sql1->lieu;
		$ville = $sql1->ville;
		$date = formater_date($sql1->date);
		$heure = formater_heure($sql1->heure);
		$prix = $sql1->prix;

		//Envoi du mail
		$message = "Bonjour,\nNous vous confirmons votre réservation pour le concert de $nom à $lieu ($ville), le $date à $heure.\nVotre réservation est valable pendant les 15 prochaines minutes.\nPour recevoir votre billet, veuillez payer la somme de $prix € à l'adresse suivant : https://paypal.com/topconcert\nA très vite sur Top Concert !";
		$message = wordwrap($message, 70);
		//mail('$email', 'Top Concert : Paiement pour le concert de $nom', $message);
	}
}
else {
	header("Location:index.php");
}

?>




<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="shortcut icon" href="./img/favicon.png">
	
	<link rel="stylesheet" href="css/style-index.css">
	<link rel="stylesheet" href="css/style-confirmation.css">

	<title>Confirmation | Top Concert</title>
</head>
<body>

	<header>
		<?php require('header.php') ?>
	</header>

	<?php if (!isset($_POST['send'])): ?>
		<div class="container">
			<form action="confirmation.php?idConcert=<?=$idConcert?>" method="post">
				<h2>Réservation :</h2>
				<div>
					<p>Veuillez renseigner votre adresse email pour recevoir la procédure de paiement ainsi que le billet :</p>
					<p><input type="email" name="email" placeholder="nom@exemple.com" required></p>
				</div>
				<p><input type="submit" name="send" value="ENVOYER"></p>
			</form>
		</div>
	<?php endif; ?>

	<?php if (isset($_POST['send'])): ?>
		<div class="container">
			<h2>Confirmation :</h2>
			<div>
				<p>Nous vous confirmons votre réservation pour le concert suivant :</p>
				<p class="resume-concert"><b><?=strtoupper($nom)?></b>, à <b><?=$lieu?> (<?=$ville?>)</b>, le <b><?=$date?></b> à <b><?=$heure?></b>.</p>
			</div>
			<div>
				<p>Vous recevrez un email, à l'adresse renseignée précédemment, contenant toutes les informations liées au paiement afin de recevoir votre billet.</p>
				<p><b>Attention :</b> votre réservation est valable pendant les 15 prochaines minutes. Vous devez procéder au paiement avant.</p>
			</div>
			<div>
				<p>Nous vous remercions pour votre confiance, à très vite sur Top Concert !</p>
			</div>
			<a href="index.php">RETOUR ACCUEIL</a>
		</div>
	<?php endif; ?>

	<footer>
		<?php require('footer.php') ?>
	</footer>
	
</body>
</html>